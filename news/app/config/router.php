<?php

$router = $di->getRouter();

// Define your routes here
$router->add(
    '/admin/users/my-profile',
    [
        'controller' => 'users',
        'action'     => 'profile',
    ]
);

// Define your routes here
$router->add(
    '/admin/users/index',
    [
        'controller' => 'users',
        'action'     => 'index',
    ]
);

$router->handle($_SERVER['REQUEST_URI']);
